const mongoose = require('mongoose');

exports.errorHandler = () => async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    if (err instanceof mongoose.Error.ValidationError) {
      ctx.status = 400;
      ctx.body = {
        invalidAttributes: {},
        status: 400
      };

      for (let field in err.errors) {
        if (err.errors.hasOwnProperty(field)) {
          let error = err.errors[field];
          ctx.body.invalidAttributes[field] = [{ rule: error.kind, message: error.message }];
        }
      }
    } else {
      ctx.status = err.status || 500;
      ctx.body = { msg: (err.expose && err.message) ? err.message : 'Error performing action' };
    }
    ctx.app.emit('error', err, ctx);
  }
};
